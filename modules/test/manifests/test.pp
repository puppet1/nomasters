class test::test (
  Optional[String[1]] $test_string = undef,
  $domain = lookup('domain', undef, undef, 'test.com'),
  $vms = {}
) {

  file{'/root/puppettest.txt':
    content =>  "It works! \n domain: ${domain}"
  }


  file { '/root/hiera_test.txt':
    ensure  => file,
    content => @("END"),


               Data from test::test
               -----
               profile::hiera_test::test_string: ${test_string}

               |END
    owner   => root,
    mode    => '0644',
  }

  create_resources('hypervisor::vm', $vms)

}
